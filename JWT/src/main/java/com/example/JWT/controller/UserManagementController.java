package com.example.JWT.controller;


import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.JWT.service.JwtUserDetailService;

@RestController
@CrossOrigin
@Scope("prototype")
public class UserManagementController {

    @Autowired
    private JwtUserDetailService userDetailsService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private AuthenticationManager authenticationManager;



    @RequestMapping(value = "/generateToken", method = RequestMethod.POST, headers = "Accept=application/json", consumes = "application/json")
    public ResponseEntity<Object> generateToken(@RequestBody Map<String, String> reqBodyMap,
                                                HttpSession session) throws Exception {

        final String token = jwtTokenUtil.generateToken(reqBodyMap.get("username"),reqBodyMap.get("role"));
        session.setAttribute("username", reqBodyMap.get("username"));
        session.setAttribute("role",reqBodyMap.get("role"));
        return ResponseEntity.ok(new JwtResponse(token));
    }

    @GetMapping(value="/username")
    public ResponseEntity<Object> getUsername(){
        SecurityContext sc = SecurityContextHolder.getContext();
        UserDetails user = (UserDetails) sc.getAuthentication().getPrincipal();

        return new ResponseEntity(user.getUsername(),HttpStatus.OK);
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            Authentication authentication= authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            SecurityContextHolder.getContext().setAuthentication(authentication);
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

}

